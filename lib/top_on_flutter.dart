import 'dart:async';

import 'package:flutter/services.dart';

class TopOnFlutter {
  static const MethodChannel _channel =
      const MethodChannel('top_on_flutter');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
